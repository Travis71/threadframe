package controller;

import java.io.File;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

/**
 *
 * @author Jadpa17
 */
public class MainTreeController {

    private DefaultTreeModel dtm;

    public DefaultTreeModel getDTM(String path) {
        File file = new File(path);
        DefaultMutableTreeNode root = null;
        if (file.isFile()) {
            DefaultMutableTreeNode node = null;
            root = new DefaultMutableTreeNode(file.getParent());
            node = new DefaultMutableTreeNode(file.getName());
            root.add(node);
        } else {
            File[] listFiles = file.listFiles();
            root = new DefaultMutableTreeNode(file.getName());
            root = forFiles(listFiles, root);
            dtm = new DefaultTreeModel(root);
        }
        return dtm;
    }

    private DefaultMutableTreeNode forFiles(File[] listFiles, DefaultMutableTreeNode nodeFather) {
        DefaultMutableTreeNode node = null;
        DefaultMutableTreeNode temp = nodeFather;
        for (File f : listFiles) {
            node = new DefaultMutableTreeNode(f.getName());
            if (f.isDirectory()){
                if(f.listFiles() != null) node = forFiles(f.listFiles(),node);
                else node.add(new DefaultMutableTreeNode("Vacio"));
            }
            temp.add(node);
        }
        return temp;
    }
}